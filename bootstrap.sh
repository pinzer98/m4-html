#!/bin/sh
apt-get update

apt-get install ruby-full build-essential zlib1g-dev -y


echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc


gem install jekyll bundler

bundle init

echo 'gem "jekyll" >> ./Gemfile'

bundle

cd /vagrant/blogalbert/



#jekyll serve --host 192.168.100.10 --livereload &
#vagrant rsync-back
